<?php


/**
 * App Routes
 */
Route::group([], function() {
    Route::get('/', function () {
        return view('web_app.index');
    });

    /**
     * 1. Home
     */
    Route::get('/admin', function () {
        return redirect()->to('admin/login');
    });


    /**
     * 2. Images route
     */
    Route::get('images/sliders/{folder}/{filename}', function ($folder, $filename) {
        $path = storage_path()
            . DIRECTORY_SEPARATOR
            . 'app'
            . DIRECTORY_SEPARATOR
            . 'images'
            . DIRECTORY_SEPARATOR
            . 'sliders'
            . DIRECTORY_SEPARATOR
            . $folder
            . DIRECTORY_SEPARATOR
            . $filename;

        if (!File::exists($path)) abort(404);

        $file = File::get($path);
        $type = File::mimeType($path);

        $response = Response::make($file, 200);
        $response->header("Content-Type", $type);

        return $response;
    });

    /**
     * 2. Products route
     */

    Route::group(['prefix' => 'images/products'], function () {
        $path = storage_path()
            . DIRECTORY_SEPARATOR
            . 'app'
            . DIRECTORY_SEPARATOR
            . 'images'
            . DIRECTORY_SEPARATOR
            . 'products'
            . DIRECTORY_SEPARATOR;

        Route::get('{folder}/{filename}', function ($folder, $filename) use ($path) {
            $path .= $folder . DIRECTORY_SEPARATOR . $filename;
            if (!File::exists($path)) abort(404);

            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        });

        Route::get('{folder}/small/{filename}', function ($folder, $filename) use ($path) {
            $path .= $folder . DIRECTORY_SEPARATOR . 'small' . DIRECTORY_SEPARATOR . $filename;
            if (!File::exists($path)) abort(404);

            $file = File::get($path);
            $type = File::mimeType($path);

            $response = Response::make($file, 200);
            $response->header("Content-Type", $type);
            return $response;
        });
    });
});

/**
 * Admin App Routes
 */
Route::group(['prefix' => 'admin'], function () {

    /**
     * 3. Auth Routes
     */
    // Authentication Routes...
    $this->get('login', 'Auth\LoginController@showLoginForm')->name('login');
    $this->post('login', 'Auth\LoginController@login');
    $this->get('logout', 'Auth\LoginController@logout');

    // Registration Routes...
    $this->get('register', 'Auth\RegisterController@showRegistrationForm');
    $this->post('register', 'Auth\RegisterController@register');

    // Password Reset Routes...
    $this->get('password/reset', 'Auth\ForgotPasswordController@showLinkRequestForm');
    $this->post('password/email', 'Auth\ForgotPasswordController@sendResetLinkEmail');
    $this->get('password/reset/{token}', 'Auth\ResetPasswordController@showResetForm');
    $this->post('password/reset', 'Auth\ResetPasswordController@reset');

    /**
     * 4. HomeController
     */
    Route::get('/home', 'HomeController@index');

    /**
     * 5. Sliders
     */
    Route::get('home/sliders', [
        'as' => 'home.sliders',
        'uses' => 'HomeController@sliders'
    ]);

    /**
     * 6. Passport
     */
    Route::group(['prefix' => '/passport'], function () {

        Route::get('authorized-clients', [
            'as' => 'passport.authorized-clients',
            'uses' => 'HomeController@authorizedClients'
        ]);
    });

    /**
     * 7. Products
     */
    Route::get('/products', [
        'as' => 'home.products',
        'uses' => 'HomeController@products'
    ]);

    /**
     * 8. Categories
     */
    Route::get('home/categories', [
        'as' => 'home.categories',
        'uses' => 'HomeController@categories'
    ]);
});