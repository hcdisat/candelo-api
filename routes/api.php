<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['middleware' => ['api', 'cors']], function() {
    Route::get('/user', function (Request $request) {
        return $request->user();
    });

    Route::resource('/sliders', 'Api\SliderController');
    Route::resource('/categories', 'Api\CategoriesController');
    Route::resource('/products', 'Api\ProductsController');
    Route::resource('/social-media', 'Api\SocialMediaController');
    Route::post('/products/update-image/{product}', 'Api\ProductsController@updateImage');

    Route::resource('/config', 'Api\ConfigurationController', [
        'only' => ['index', 'show', 'update']
    ]);
});