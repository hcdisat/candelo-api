<?php

use App\Hcdisat\Services\SliderService;
use App\Hcdisat\Tests\SliderTestCase;
use App\Models\Slider;
use App\Repositories\Contracts\SliderRepository;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SliderServiceTest extends SliderTestCase
{
    use DatabaseMigrations;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var SliderRepository
     */
    protected $repository;

    public function setUp()
    {
        parent::setUp();
        $this->data = [
            'title' => 'Brand new Slider New New',
            'order' => 3,
            'boxed' => false,
            'centered' => false,
            'content' => 'Some random text.',
            'image' => app(Illuminate\Http\UploadedFile::class, [
                '/tmp/some_promo.jpg',
                'some_promo.jpg',
                'image/jpeg',
                filesize('/tmp/some_promo.jpg'),
                null,
                true
            ]),
            'button' => [
                'show' => true,
                'text' => 'Click Me!',
                'url' => 'home',
            ],
        ];

//        $this->repository = app(SliderRepository::class);
    }

    public function test_create()
    {
        $service = app(SliderService::class);
        $service->create($this->data);

        $this->seeInDatabase('sliders', ['title' => 'Brand new Slider New New']);
    }

    public function test_update()
    {
        $this->generateSlider();
        $service = app(SliderService::class);
        $this->data['title'] = 'Updated';
        $service->update($this->data, 1);

        $this->assertEquals($this->data['title'], Slider::first()->title);
    }
}
