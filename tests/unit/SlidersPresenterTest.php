<?php

use App\Hcdisat\Tests\SliderTestCase;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SlidersPresenterTest extends SliderTestCase
{
    use DatabaseMigrations;

    public function test_background_url()
    {
        $slider = $this->generateSlider();
        $slider = new \App\ViewPresenters\SlidersPresenter($slider);
        $this->assertEquals('http://localhost/images/1/1.jpg', $slider->backgroundUrl);
    }
}
