<?php


/**
 * Created by PhpStorm.
 * User: hcdisat
 * Date: 29/10/16
 * Time: 09:46 PM
 */
class ApiProductsControllerTest extends \App\Hcdisat\Tests\ProductTestCase
{
    use \Illuminate\Foundation\Testing\WithoutMiddleware;

    /**
     * @var string
     */
    protected $resource;

    protected function setUp()
    {
        parent::setUp();
        $this->resource = '/api/products';
    }

    public function test_response_is_not_null()
    {
        $this->assertNotEmpty($this->response);
        $this->assertCount(2, $this->response);
    }

    public function test_index()
    {
        $this->get($this->resource)
            ->seeStatusCode(200)
            ->seeJsonStructure([
                '*' => [
                    'category_id',
                    'description',
                    'created_at',
                    'name'
                ]
            ]);
    }

    public function test_show()
    {
        $this->get($this->resource.DIRECTORY_SEPARATOR.'1')
            ->seeStatusCode(200)
            ->seeJsonStructure([
                '*' => [
                    'category_id',
                    'description',
                    'created_at',
                    'name'
                ]
            ]);
    }

    public function test_update_with_picture()
    {
        $payload = [
            'category_id' => 6,
            'description' => 'Ut est laboriosam odio iusto quam eum et minus. 
                Esse eos maiores dolorem occaecati aut. Nemo in et nostrum.',
            'price' => '245.99',
            'image' => $this->prepareFileUpload(config('testing.assets.slider_image'), 'some_promo.jpg', 'image/jpg')
        ];

        $this->put($this->resource.DIRECTORY_SEPARATOR.'1', $payload)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => null,
                'message' => 'product updated.',
                'status' => 200
            ]);
    }

    public function test_update_without_picture()
    {
        $payload = [
            'category_id' => 6,
            'description' => 'Ut est laboriosam odio iusto quam eum et minus. 
                Esse eos maiores dolorem occaecati aut. Nemo in et nostrum.',
            'price' => '250.99'
        ];

        $this->put($this->resource.DIRECTORY_SEPARATOR.'1', $payload)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => null,
                'message' => 'product updated.',
                'status' => 200
            ]);

        $this->assertEquals(250.99, \App\Models\Product::find(1)->price);
    }

    public function test_delete()
    {
        $this->delete("{$this->resource}/1")
            ->seeStatusCode(200)
            ->seeJson([
                'data' => null,
                'message' => 'product deleted.',
                'status' => 200
            ]);
    }

    public function test_store()
    {
        $payload = [
            'category_id' => 6,
            'name' => 'New Product',
            'description' => 'Ut est laboriosam odio iusto quam eum et minus. Esse eos maiores dolorem occaecati aut. Nemo in et nostrum.',
            'price' => '245.36',
            'image' => $this->prepareFileUpload(config('testing.assets.slider_image'), 'some_promo.jpg', 'image/jpg')
        ];

        $this->post($this->resource, $payload)
            ->seeStatusCode(201)
            ->seeJson([
                'data' => null,
                'message' => 'product created.',
                'status' => 201
            ]);

        $this->delete($this->resource.DIRECTORY_SEPARATOR.'3')
            ->seeStatusCode(200)
            ->seeJson([
                'data' => null,
                'message' => 'product deleted.',
                'status' => 200
            ]);
    }

    public function test_update_image()
    {
        $payload = [
            'image' => $this->prepareFileUpload(
                config('testing.assets.slider_image'),
                'some_promo.jpg', 'image/jpg'
            )
        ];

        $this->post($this->resource.'/update-image/1', $payload)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => null,
                'message' => 'image updated.',
                'status' => 200
            ]);
    }
}
