<?php

class ApiCategoryControllerTest extends \App\Hcdisat\Tests\TestCase
{
    protected $resource;

    protected function setUp()
    {
        parent::setUp();
        $this->resource = '/api/categories';
        $order = [
            'id',
            'name',
            'description',
            'created_at',
            'updated_at'
        ];
        $this->response = $this->getData($order, 'category_response.json');
        $this->generateData(\App\Models\Category::class);
    }


    public function test_index()
    {
        $response = $this->response;
        $this->get($this->resource)
            ->seeStatusCode(200)
            ->seeJson($response);

    }

    public function test_show()
    {
        $data = $this->response[0];
        $this->get($this->resource.DIRECTORY_SEPARATOR.'1')
            ->seeStatusCode(200)
            ->seeJson($data);
    }

    public function test_store()
    {
        $payload = $this->getModelData()[0];
        $payload['name'] = 'New Category';
        unset($payload['id']);
        unset($payload['created_at']);
        unset($payload['updated_at']);

        $this->post($this->resource, $payload)
            ->seeStatusCode(201)
            ->seeJson([
                'data' => null,
                'message' => 'Category created.',
                'status' => 201
            ]);

        $last = \App\Models\Category::all()->last();
        $this->assertEquals($payload['name'], $last->name);
    }

    public function test_update()
    {
        $payload = $this->getModelData()[0];
        $payload['name'] = 'Updated Category';
        unset($payload['id']);
        unset($payload['created_at']);
        unset($payload['updated_at']);

        $this->put($this->resource.DIRECTORY_SEPARATOR.'1', $payload)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => null,
                'message' => 'Category updated.',
                'status' => 200
            ]);

        $categoryOne = \App\Models\Category::find(1);
        $this->assertEquals($payload['name'], $categoryOne->name);
    }

    public function test_delete()
    {
        $this->delete($this->resource.DIRECTORY_SEPARATOR.'1')
            ->seeStatusCode(200)
            ->seeJson([
                'data' => null,
                'message' => 'Category deleted.',
                'status' => 200
            ]);
        $this->assertCount(1, \App\Models\Category::all());
    }

    protected function getModelData()
    {
        return [
            [
                'id' => 1,
                'name' => 'Von and Sons',
                'description' => 'Temporibus est nam quo rerum et quia labore ut. Ratione esse sit non fugit est. Laboriosam libero eaque eaque fugit.',
                'created_at' => '2016-11-06 02:31:07',
                'updated_at' => '2016-11-06 02:31:07',
            ],
            [
                'id' => 2,
                'name' => 'Lynch, Collins and Ullrich',
                'description' => 'Repellat in ex aut eum. Quas cumque sapiente aut minima. Esse aut quaerat suscipit labore aliquam.',
                'created_at' => '2016-11-06 02:31:07',
                'updated_at' => '2016-11-06 02:31:07',
            ]
        ];
    }
}
