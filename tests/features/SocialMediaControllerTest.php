<?php

use App\Models\SocialNetworks;

class SocialMediaControllerTest extends \App\Hcdisat\Tests\TestCase
{
    /** @var  string */
    protected $resource;

    /** @var  array */
    protected $order;

    protected function setUp()
    {
        parent::setUp();
        $this->resource = '/api/social-media';
        $this->order = array_keys($this->getModelData()[0]);
        $this->response = $this->getData($this->order, 'social-media_response.json');
        $this->generateData(SocialNetworks::class);
    }

    public function test_index()
    {
        $this->get($this->resource)
            ->seeStatusCode(200)
            ->seeJsonStructure(['*' => $this->order]);
    }

    public function test_show()
    {
        $this->get("{$this->resource}/1")
            ->seeStatusCode(200)
            ->seeJsonStructure($this->order);
    }

    public function test_update()
    {
        $payload = [
            'body' => 'Updated',
            'url' => '#',
        ];

        $this->put("{$this->resource}/1", $payload)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => null,
                'message' => 'Social Network updated.',
                'status' => 200
            ]);

        $this->assertEquals($payload['body'], SocialNetworks::find(1)->body);
        $this->assertEquals($payload['url'], SocialNetworks::find(1)->url);
    }

    public function test_destroy()
    {
        $this->delete("{$this->resource}/1")
            ->seeStatusCode(200)
            ->seeJson([
                'data' => null,
                'message' => 'Social Network deactivated.',
                'status' => 200
            ]);
    }

    /**
     * @return array
     */
    protected function getModelData()
    {
        return [
            [
                'id' => 1,
                'name' => 'twitter',
                'title' => 'Twitter Marketing',
                'body' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.',
                'url' => 'http://localhost:8080',
            ],
            [
                'id' => 2,
                'name' => 'facebook',
                'title' => 'Facebook Marketing',
                'body' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.',
                'url' => 'http://localhost:8080',
            ]
        ];
    }
}
