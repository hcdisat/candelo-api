<?php

use App\Hcdisat\Tests\SliderTestCase;
use App\Hcdisat\Traits\UploadFileTrait;
use App\Models\Slider;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class ApiSliderControllerTest extends SliderTestCase
{
    use DatabaseMigrations, WithoutMiddleware, UploadFileTrait;

    public function setUp()
    {
        parent::setUp();
        for ($i = 1; $i <= 2; $i++) {
            $this->generateSlider();
        }
    }

    public function test_index()
    {
        $this->get('/api/sliders')
            ->seeJsonStructure([
                '*' => [
                    'id', 'background',
                    'button', 'order'
                ]
            ]);
    }

    public function test_show()
    {
//        $this->get('/api/sliders/2')->dump();
        $this->get('/api/sliders/2')
            ->seeJsonStructure([
                'id', 'background',
                'button', 'order'
            ]);
    }

    public function test_store()
    {
        $payload = [
            'title' => 'new Slider',
            'order' => 3,
            'boxed' => false,
            'centered' => false,
            'content' => 'Some random text.',
            'image' => $this->prepareFileUpload(config('testing.assets.slider_image'), 'some_promo.jpg', 'image/jpeg'),
            'button' => [
                'show' => true,
                'text' => 'Click Me!',
                'url' => 'home'
            ]
        ];
        $this->post('/api/sliders', $payload)->dump();
        $this->post('/api/sliders', $payload)
            ->seeStatusCode(200)
            ->seeJson([
                'created' => true
            ]);

        $filePath = storage_path()
            .DIRECTORY_SEPARATOR
            .'app'
            .DIRECTORY_SEPARATOR
            .'images'
            .DIRECTORY_SEPARATOR
            .'sliders'
            .DIRECTORY_SEPARATOR
            .'3'
            .DIRECTORY_SEPARATOR
            .'3.jpg';
        $this->assertFileExists($filePath);
    }

    public function test_update()
    {
        $payload = [
            'id' => 1,
            'title' => 'new Slider2',
            'order' => 1,
            'boxed' => true,
            'centered' => true,
            'content' => 'Some random text.',
            'image' => $this->prepareFileUpload('/tmp/some_promo.jpg', 'some_promo.jpg', 'image/jpeg'),
            'button' => [
                'show' => true,
                'text' => 'Click Me2!',
                'url' => 'home'
            ]
        ];

        $this->put('/api/sliders/1', $payload)
            ->seeStatusCode(200)
            ->seeJson(['updated' => true]);

        $slider = Slider::find(1);
        $this->assertEquals($payload['title'], $slider->title);
        $this->assertEquals(1, $slider->boxed);
    }

    public function test_delete()
    {
        $this->delete('/api/sliders/1')
            ->seeStatusCode(200)
            ->seeJson(['deleted' => true]);
    }
}
