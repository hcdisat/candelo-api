<?php

use App\Models\Configuration;

class ConfigurationControllerTest extends \App\Hcdisat\Tests\TestCase
{
    /** @var  string */
    protected $resource;

    /** @var  array */
    protected $order;

    protected function setUp()
    {
        parent::setUp();
        $this->resource = '/api/config';
        $this->order = array_keys($this->getModelData()[0]);
        $this->response = $this->getData($this->order, 'configuration_response.json');
        $this->generateData(Configuration::class);
    }

    public function test_index()
    {
        $this->get($this->resource)
            ->seeStatusCode(200)
            ->seeJsonStructure(['*' => $this->order]);
    }

    public function test_show()
    {
        $this->get("{$this->resource}/1")
            ->seeStatusCode(200)
            ->seeJsonStructure($this->order);
    }

    public function test_update()
    {
        $payload = [
            'value' => '1.0.1',
        ];

        $this->put("{$this->resource}/1", $payload)
            ->seeStatusCode(200)
            ->seeJson([
                'data' => null,
                'message' => 'value updated.',
                'status' => 200
            ]);

        $this->assertEquals($payload['value'], Configuration::find(1)->value);
    }

    /**
     * @return array
     */
    protected function getModelData()
    {
        return [
            [
                'id' => 1,
                'name' => 'version',
                'value' => '0.1-dev',
                'description' => 'App version, serves to keep a control of the current running instance',
            ],
            [
                'id' => 2,
                'name' => 'commit',
                'value' => '57337e4',
                'description' => 'development',
            ],
        ];
    }
}
