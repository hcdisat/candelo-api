<?php

return [

    /*
     * Static assets for tests
     */
    'assets' => [
        'slider_image' => '/tmp/some_promo.jpg'
    ]
];