@extends('layouts.app')
@section('contentheader_title', trans('passport.title'))
@section('main-content')
    <!-- Box Comment -->
    <div class="box box-widget">
        <div class="box-header with-border">
            <div class="user-block">
                <span class="username">
                    {{ trans('passport.subtitle') }}
                </span>
            </div>
            <!-- /.user-block -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- Attachment -->
            <div class="attachment-block clearfix">
                <div class="attachment-pushed">
                    <h4 class="attachment-heading">{{ trans('passport.authorized-clients') }}</h4>

                    <div class="attachment-text">
                        <authorized-clients></authorized-clients>
                    </div>
                    <!-- /.attachment-text -->
                </div>
                <!-- /.attachment-pushed -->
            </div>
            <!-- /.attachment-block -->
        </div>
        <!-- Attachment -->
        <div class="attachment-block clearfix">
            <div class="attachment-pushed">
                <h4 class="attachment-heading">{{ trans('passport.clients') }}</h4>

                <div class="attachment-text">
                    <clients></clients>
                </div>
                <!-- /.attachment-text -->
            </div>
            <!-- /.attachment-pushed -->
        </div>
        <!-- /.attachment-block -->
        <!-- Attachment -->
        <div class="attachment-block clearfix">
            <div class="attachment-pushed">
                <h4 class="attachment-heading">{{ trans('passport.personal-tokens') }}</h4>

                <div class="attachment-text">
                    <personal-access-tokens></personal-access-tokens>
                </div>
                <!-- /.attachment-text -->
            </div>
            <!-- /.attachment-pushed -->
        </div>
        <!-- /.attachment-block -->
    </div>
    </div>
    </div>
    <!-- /.box -->
@endsection