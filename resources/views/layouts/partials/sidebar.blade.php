<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">

    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        @if (! Auth::guest())
            <div class="user-panel">
                <div class="pull-left image">
                    <img src="{{asset('/img/user2-160x160.jpg')}}" class="img-circle" alt="User Image"/>
                </div>
                <div class="pull-left info">
                    <p>{{ Auth::user()->name }}</p>
                    <!-- Status -->
                    <a href="#"><i class="fa fa-circle text-success"></i> {{ trans('message.online') }}</a>
                </div>
            </div>
        @endif

    <!-- search form (Optional) -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="{{ trans('message.search') }}..."/>
                <span class="input-group-btn">
                <button type='submit' name='search' id='search-btn' class="btn btn-flat"><i
                            class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->

        <!-- Sidebar Menu -->
        <ul class="sidebar-menu">
            <li class="header">{{ trans('message.header') }}</li>
            <li class="active">
                <a href="{{ url('admin/home') }}">
                    <i class='fa fa-home'></i>
                    <span>{{ trans('message.home') }}</span>
                </a>
            </li>
            <li class="active">
                <a href="{{ route('passport.authorized-clients') }}">
                    <i class='fa fa-lock'></i>
                    <span>{{ trans('message.passport') }}</span>
                </a>
            </li>
            <li class="active">
                <a href="{{ route('home.sliders') }}">
                    <i class='fa fa-image'></i>
                    <span>{{ trans('message.sliders') }}</span>
                </a>
            </li>
            <li class="active">
                <a href="{{ route('home.products') }}">
                    <i class='fa fa-image'></i>
                    <span>{{ trans('message.products') }}</span>
                </a>
            </li>
            <li class="active">
                <a href="{{ route('home.categories') }}">
                    <i class='fa fa-image'></i>
                    <span>{{ trans('message.categories') }}</span>
                </a>
            </li>
            <li><a href="#"><i class='fa fa-link'></i> <span>{{ trans('message.anotherlink') }}</span></a></li>
            <li class="treeview">
                <a href="#">
                    <i class='fa fa-link'></i> <span>{{ trans('message.multilevel') }}</span>
                    <i
                            class="fa fa-angle-left pull-right"></i></a>
                <ul class="treeview-menu">
                    <li><a href="#">{{ trans('message.linklevel2') }}</a></li>
                    <li><a href="#">{{ trans('message.linklevel2') }}</a></li>
                </ul>
            </li>
        </ul><!-- /.sidebar-menu -->
    </section>
    <!-- /.sidebar -->
</aside>
