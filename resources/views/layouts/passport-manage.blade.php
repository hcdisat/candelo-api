@extends('layouts.master')
@section('title', 'App Passport')
@section('content')
    <!-- Box Comment -->
    <div class="box box-widget">
        <div class="box-header with-border">
            <div class="user-block">
                <span class="username">
                    Administracion de acceso a la API
                </span>
            </div>
            <!-- /.user-block -->
        </div>
        <!-- /.box-header -->
        <div class="box-body">
            <!-- Attachment -->
            <div class="attachment-block clearfix">
                <div class="attachment-pushed">
                    <h4 class="attachment-heading">{{ $title }}</h4>

                    <div class="attachment-text">
                        <authorized-clients></authorized-clients>
                    </div>
                    <!-- /.attachment-text -->
                </div>
                <!-- /.attachment-pushed -->
            </div>
            <!-- /.attachment-block -->
        </div>
    </div>
    <!-- /.box -->
@endsection