@extends('layouts.app')

@section('contentheader_title', 'Sliders')
@section('main-content')
    {{--<div class="box box-primary">--}}
        {{--<div class="box-header with-border">--}}
            {{--<h3 class="box-title">Administración de Sliders</h3>--}}

            {{--<div class="box-tools pull-right">--}}
                {{--<button type="button" class="btn btn-box-tool" data-widget="collapse"><i class="fa fa-minus"></i>--}}
                {{--</button>--}}
                {{--<button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>--}}
            {{--</div>--}}
        {{--</div>--}}
        {{--<!-- /.box-header -->--}}
        {{--<div class="box-body">--}}
            {{--<ul class="products-list product-list-in-box">--}}
                {{--@foreach($sliders as $slider)--}}
                    {{--<li class="item">--}}
                        {{--<div class="product-img">--}}
                            {{--<img src="{{ $slider->backgroundUrl }}" alt="{{ $slider->background->name }}">--}}
                        {{--</div>--}}
                        {{--<div class="product-info">--}}
                            {{--<div class="product-title">{{ $slider->title }}--}}
                                {{--<a href="#">--}}
                                    {{--<span class="control-box label label-danger pull-right"><i class="fa fa-trash"></i></span>--}}
                                {{--</a>--}}
                                {{--<a href="#">--}}
                                    {{--<span class="control-box label label-warning pull-right"><i class="fa fa-pencil"></i></span>--}}
                                {{--</a>--}}
                            {{--</div>--}}
                            {{--<span class="product-description">--}}
                          {{--{{ $slider->content or 'Empty' }}--}}
                        {{--</span>--}}
                        {{--</div>--}}
                    {{--</li>--}}
                    {{--<!-- /.item -->--}}
                {{--@endforeach--}}
            {{--</ul>--}}
        {{--</div>--}}
    {{--</div>--}}
    <sliders></sliders>
@endsection