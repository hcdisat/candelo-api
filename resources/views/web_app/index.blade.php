<!DOCTYPE html>
<html lang=en>
<head>
    <meta charset=utf-8>
    <meta http-equiv=X-UA-Compatible content="IE=edge">
    <title>Focal html5 portfolio Template</title>
    <meta name=viewport content="width=device-width,initial-scale=1">
    <meta name=description content="">
    <meta name=author content="Hector Anibal Caraballo Barreiro">
    <title>Home | Candelo</title>
    <link rel=stylesheet href=./static/css/bootstrap.min.css>
    <link rel=stylesheet href=./static/css/font-awesome.css>
    <link rel=stylesheet href=./static/css/flaticon.css>
    <link rel=stylesheet href="https://fonts.googleapis.com/icon?family=Material+Icons">
    <link rel=stylesheet href=./static/css/keen-ui.css>
    <link rel=stylesheet href=./static/css/prettyPhoto.css>
    <link rel=stylesheet href=./static/css/animate.css>
    <link rel=stylesheet href=./static/css/main.css>
    <link rel="shortcut icon" href=./static/images/ico/favicon.ico>
    <link rel=apple-touch-icon-precomposed sizes=144x144 href=./static/images/ico/apple-touch-icon-144-precomposed.png>
    <link rel=apple-touch-icon-precomposed sizes=114x114 href=./static/images/ico/apple-touch-icon-114-precomposed.png>
    <link rel=apple-touch-icon-precomposed sizes=72x72 href=./static/images/ico/apple-touch-icon-72-precomposed.png>
    <link rel=apple-touch-icon-precomposed href=./static/images/ico/apple-touch-icon-57-precomposed.png>
    <link href=/static/css/app.2691e095a118b7df8c67985f42687fa3.css rel=stylesheet>
</head>
<body>
<app id=app></app>
<script src=./static/js/jquery.js></script>
<script src=./static/js/bootstrap.min.js></script>
<script src=./static/js/main.js></script>
<script>
    window.Laravel = { csrfToken: '{{ csrf_token() }}' };
</script>
<script type=text/javascript src=/static/js/manifest.8cceeb582c6aec0ca939.js></script>
<script type=text/javascript src=/static/js/vendor.3090f45e44a8e2f2f010.js></script>
<script type=text/javascript src=/static/js/app.f4ef5e9fa81d29e79541.js></script>
</body>
</html>