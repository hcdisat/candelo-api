import Service from './../../core/Service'
import CategoryService from './../categories/CategoryService'

/**
 * updates an image
 * @param id
 * @param data
 * @param successFn
 * @param errorFn
 */
let updateImage = (id, data, successFn, errorFn) => {
    if (!data instanceof FormData) {
        return;
    }

    Vue.http.post(`/api/products/update-image/${id}`, data)
        .then(
            response => {
                ProductService.success(successFn, response);
            },
            error => {
                ProductService.errorResponse(errorFn, error);
            }
        );
};


let ProductService = new Service('/api/products');
ProductService.Category = CategoryService;
ProductService.updateImage = updateImage;

export default ProductService;