/**
 * Global Components
 */
export default {
    AuthorizedClients: require('./passport/AuthorizedClients.vue'),
    Clients: require('./passport/Clients.vue'),
    PersonalAccessTokens: require('./passport/PersonalAccessTokens.vue'),
    Sidebar: require('./Sidebar.vue'),
    Sliders: require('./sliders/Sliders.vue'),
    Products: require('./products/Products.vue'),
    Category: require('./categories/Category.vue'),
    CategoryForm: require('./categories/CategoryForm.vue'),
}
