export default {
    uploader: {
        update() {
            Vue.nextTick(() => {
                $(document).ready(() => {
                    $(this.el).fileinput({
                        theme: "fa",
                        showPreview: false,
                        showUpload: false,
                        language: 'es',
                        previewFileType: 'any',
                        allowedFileExtensions: ['jpg', 'png', 'gif'],
                    });
                });
            });
        }
    }
}