require('./bootstrap');

import Keen from 'keen-ui'
Vue.use(Keen);

/**
 * Load Components
 */
import components from './components/index'
Object.keys(components).forEach(c => Vue.component(c, components[c]));

import directives from './directives/index'
Object.keys(directives).forEach(d => Vue.directive(d, directives[d]));

new Vue({
    el: 'body'
});