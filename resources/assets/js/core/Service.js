export default (resource) => {

    return {
        /**
         * @var string
         */
        resource: resource,

        /**
         * @boolean
         */
        hasErrors: false,

        error: {
            name
        },

        /**
         * @array
         */
        errors (error) {
            let errorsName = Object.keys(error);
            errorsName.forEach(k => {
                if (Array.isArray(error[k])) {
                    this.error[k] = error[k].join('<br />');
                }
            });
        },

        /**
         * fetch all categories
         * @param successFn
         * @param errorFn
         */
        all(successFn, errorFn) {
            Vue.http.get(this.resource).then(
                response => {
                    this.success(successFn, response);
                },
                error => {
                    this.errorResponse(errorFn, error.data);
                }
            );
        },

        /**
         *
         * @param data
         * @param successFn
         * @param errorFn
         */
        store(data, successFn, errorFn) {
            Vue.http.post(this.resource, data).then(
                response => {
                    this.success(successFn, response);
                },
                error => {
                    this.errorResponse(errorFn, error);
                }
            );

        },

        /**
         *
         * @param id
         * @param successFn
         * @param errorFn
         */
        show(id, successFn, errorFn) {
            Vue.http.get(this.getUrl(id)).then(
                response => {
                    this.success(successFn, response);
                },
                error => {
                    this.errorResponse(errorFn, error);
                }
            );
        },

        /**
         *
         * @param id
         * @param data
         * @param successFn
         * @param errorFn
         */
        put(id, data, successFn, errorFn) {
            Vue.http.put(this.getUrl(id), data).then(
                response => {
                    this.success(successFn, response);
                },
                error => {
                    this.errorResponse(errorFn, error);
                }
            );
        },

        /**
         *
         * @param id
         * @param successFn
         * @param errorFn
         */
        destroy(id, successFn, errorFn) {
            let payload = {
                _method: 'DELETE'
            };
            Vue.http.post(this.getUrl(id), payload).then(
                response => {
                    this.success(successFn, response);
                },
                error => {
                    this.errorResponse(errorFn, error);
                }
            );
        },

        /**
         * validates if fn is a function
         * @param fn
         * @param args
         * @returns {boolean}
         */
        callFn(fn, ...args) {
            if (_.isFunction(fn)) {
                return fn(...args);
            }
        },

        /**
         *
         * @param id
         * @returns {string}
         */
        getUrl(id) {
            return `${this.resource}/${id}`;
        },

        /**
         *
         * @param successFn
         * @param response
         */
        success(successFn, response) {
            this.hasErrors = false;
            this.error = {
                name: ''
            };
            this.callFn(successFn, response);
        },

        errorResponse(errorFn, error) {
            this.hasErrors = true;
            this.errors(error.data);
            this.callFn(errorFn, error);
        }
    }
}