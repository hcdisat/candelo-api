export default {
    activate(done) {
        this.update(done);
    },
    computed: {
        limit() {
            if (this.showAll) {
                return null;
            }

            return this.top;
        },
        showLimit() {
            return this.collection.length > this.top;
        }
    },
    methods: {
        update(fn) {
            this.service.all(response => {
                this.collection = response.data;
                this.service.callFn(fn, response);
            });
        },
        saved(event) {
            this.update(() => {
                console.log(event);
                this.show.edit = this.show.add = false;
                this.alertMessage = event.message;
                this.showAlert();
            });
        },
        showAlert(time) {
            this.show.alertSuccess = true;
            setTimeout(() => {
                this.show.alertSuccess = false;
            }, time || 3000)
        },
        markForEdit(selected) {
            this.selected = selected;
            this.show.edit = true;
        },
        markForDelete(selected) {
            this.selected = selected;
            this.alertMessage = this.deleteMessage;
            this.show.destroy = true;
        },
        destroy() {
            this.service.destroy(this.selected.id, () => {
                this.update(this.showAlert);
            });
        },
        deleteDenied() {
            this.selected = {};
        },
    },
}