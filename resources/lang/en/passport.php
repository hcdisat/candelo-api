<?php
/**
 * Created by PhpStorm.
 * User: hcdisat
 * Date: 29/10/16
 * Time: 01:15 PM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Passport Section
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'title' => 'App Passport',
    'subtitle' => 'API access Administration',
    'authorized-clients' => 'Authorized Clients',
    'clients' => 'Clients',
    'personal-tokens' => 'Personal Access Tokens',

];