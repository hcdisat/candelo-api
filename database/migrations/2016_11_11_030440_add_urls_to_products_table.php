<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUrlsToProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        \Schema::table('products', function (Blueprint $table) {
            $table->string('url_large')->nullable()->default(null);
            $table->string('url_small')->nullable()->default(null);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        \Schema::table('products', function (Blueprint $table) {
            $table->dropColumn(['url_small', 'url_large']);
        });
    }
}
