<?php

use Illuminate\Database\Seeder;

class ConfigSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $configsDefault = [
            [
                'name' => 'version',
                'value' => '0.1-dev',
                'description' => 'App version, serves to keep a control of the current running instance',
            ],

            [
                'name' => 'commit',
                'value' => '57337e4',
                'description' => 'development',
            ],

            [
                'name' => 'main_url',
                'value' => '/',
                'description' => 'Main app\'s url, usually is /.',
            ],

            [
                'name' => 'route_blacklist',
                'value' => 'home:policy:terms:help',
                'description' => 'Routes to be removed form the navbar',
            ],

            [
                'name' => 'footer_routes',
                'value' => 'home:policy:terms:help',
                'description' => 'Routes to be displayed in the footer',
            ],

            [
                'name' => 'app_name',
                'value' => 'José Candelo',
                'description' => 'App\'s name',
            ]
        ];

        foreach ($configsDefault as $item) {
            \App\Models\Configuration::create($item);
        }
    }
}
