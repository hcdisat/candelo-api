<?php

use Illuminate\Database\Seeder;

class SocialNetworksSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $socialMedias = [
            [
                'name' => 'twitter',
                'title' => 'Twitter Marketing',
                'body' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.',
                'url' => 'http://localhost:8080',
            ],

            [
                'name' => 'facebook',
                'title' => 'Facebook Marketing',
                'body' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.',
                'url' => 'http://localhost:8080',
            ],

            [
                'name' => 'google-plus',
                'title' => 'Google Plus Marketing',
                'body' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.',
                'url' => 'http://localhost:8080',
            ],

            [
                'name' => 'pinterest',
                'title' => 'Pinterest Marketing',
                'body' => 'Pellentesque habitant morbi tristique senectus et netus et malesuada fames ac turpis egestas. Vestibulum tortor quam, feugiat vitae.',
                'url' => 'http://localhost:8080',
            ]
        ];


        foreach ($socialMedias as $socialMedia) {
            \App\Models\SocialNetworks::create($socialMedia);
        }
    }
}
