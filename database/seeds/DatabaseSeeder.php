<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        if (app()->environment() == 'testing') return;

        $categories = [
            'Veladoras',
            'Jabones',
            'Pastillas',
            'Polvos esotéricos',
            'Polvos normales',
            'Yerbas',
            'Teas',
            'Farmacéutica',
            'Santos estatuas',
            'Aguas espirituales',
            'Collares y pulseras esotéricas',
            'Inciensos',
            'Baños',
            'Lociones',
            'Oraciones o estampitas',
        ];

        foreach ($categories as $category) {
            \App\Models\Category::create([
                'name' => $category
            ]);
        }

        $this->call(ConfigSeeder::class);
        $this->call(SocialNetworksSeeder::class);
    }
}
