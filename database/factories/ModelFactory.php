<?php

use App\Models\Slider;
use App\Models\SliderBackground;
use App\Models\SliderButton;
use App\Models\User;
use Faker\Generator;

$factory->define(User::class, function($faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(Slider::class, function(Generator $faker) {

    return [
        'title' => $faker->text,
        'order' => 1,
        'boxed' => false,
        'centered' => true,
        'content' => $faker->text,
    ];
});

$factory->define(SliderBackground::class, function(Generator $faker) {
    static $id;
    $slider = factory(Slider::class)->create();
    $storageBase = storage_path()
        .DIRECTORY_SEPARATOR
        .'app'
        .DIRECTORY_SEPARATOR
        .'images'
        .DIRECTORY_SEPARATOR
        .'sliders'
        .DIRECTORY_SEPARATOR;

    $id = $id ?? 1;
    return [
        'slider_id' => $slider->id,
        'extension' => 'jpg',
        'name' => $id,
        'path' => $storageBase.$id.DIRECTORY_SEPARATOR.$id++.'.jpg',
        'url' => 'images'
            .DIRECTORY_SEPARATOR
            .'sliders'
            .DIRECTORY_SEPARATOR
            .$id.DIRECTORY_SEPARATOR.$id++.'.jpg'
    ];
});

$factory->define(SliderButton::class, function(Generator $faker) {

    return [
        'text' => $faker->text
    ];
});

$factory->define(App\Models\Product::Class, function(Generator $faker) {
    return [
        'category_id' => $faker->numberBetween(1, 30),
        'name' => $faker->unique(),
        'description' => implode('\n', $faker->paragraphs),
        'price' => $faker->numberBetween(100, 1000),
        'image_large' => 'http://lorempixel.com/360/360/sports?15258',
        'image_small' => 'http://lorempixel.com/640/640/city?19936',
    ];
});

$factory->define(\App\Models\Category::class, function(Generator $faker) {
    return [
        'name' => $faker->unique()->company,
        'description' => $faker->text
    ];
});

$factory->define(\App\Models\Configuration::class, function(Generator $faker) {
    return [
        'name' => $faker->company,
        'value' => $faker->colorName,
        'description' => $faker->text,
    ];
});

$factory->define(\App\Models\SocialNetworks::class, function(Generator $faker) {
    return [
        'name' => $faker->unique()->words(5),
        'title' => $faker->sentences,
        'body' => $faker->paragraphs,
        'url' => $faker->url,
    ];
});