<?php

namespace App\Providers;

use App\Repositories\CategoryRepositoryEloquent;
use App\Repositories\ConfigurationRepositoryEloquent;
use App\Repositories\Contracts\CategoryRepository;
use App\Repositories\Contracts\ConfigurationRepository;
use App\Repositories\Contracts\ProductRepository;
use App\Repositories\Contracts\SliderRepository;
use App\Repositories\Contracts\SocialMediaRepository;
use App\Repositories\ProductRepositoryEloquent;
use App\Repositories\SliderRepositoryEloquent;
use App\Repositories\SocialMediaRepositoryEloquent;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(SliderRepository::class, SliderRepositoryEloquent::class);
        $this->app->bind(ProductRepository::class, ProductRepositoryEloquent::class);
        $this->app->bind(CategoryRepository::class, CategoryRepositoryEloquent::class);
        $this->app->bind(ConfigurationRepository::class, ConfigurationRepositoryEloquent::class);
        $this->app->bind(SocialMediaRepository::class, SocialMediaRepositoryEloquent::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
