<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

/**
 * Class Product
 *
 * @package App\Models
 * @property integer $id
 * @property integer $category_id
 * @property string $name
 * @property string $description
 * @property float $price
 * @property string $image_large
 * @property string $image_small
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property string $url_large
 * @property string $url_small
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereCategoryId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereDescription($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product wherePrice($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereImageLarge($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereImageSmall($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereUpdatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereUrlLarge($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Product whereUrlSmall($value)
 * @mixin \Eloquent
 */
class Product extends Model implements Transformable
{
    use TransformableTrait;

    protected $guarded = ['id'];

}
