<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SliderBackground
 *
 * @property integer $id
 * @property integer $slider_id
 * @property string $name
 * @property string $extension
 * @property string $path
 * @property string $url
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderBackground whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderBackground whereSliderId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderBackground whereName($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderBackground whereExtension($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderBackground wherePath($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderBackground whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderBackground whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderBackground whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SliderBackground extends Model
{
    /**
     * @var array
     */
    protected $guarded = ['id'];
}
