<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * App\Models\SliderButton
 *
 * @property integer $id
 * @property string $text
 * @property string $url
 * @property boolean $show
 * @property integer $slider_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderButton whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderButton whereText($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderButton whereUrl($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderButton whereShow($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderButton whereSliderId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderButton whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\SliderButton whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class SliderButton extends Model
{
    /**
     * @var array
     */
    protected $guarded = ['id'];
}
