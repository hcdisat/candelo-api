<?php namespace App\Models;

use App\ViewPresenters\SlidersPresenter;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasOne;
use Robbo\Presenter\PresentableInterface;
use Robbo\Presenter\Presenter;

/**
 * App\Models\Slider
 *
 * @property integer $id
 * @property string $title
 * @property integer $order
 * @property boolean $boxed
 * @property boolean $centered
 * @property string $content
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * @property-read \App\Models\SliderBackground $background
 * @property-read \App\Models\SliderButton $button
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereId($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereTitle($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereOrder($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereBoxed($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereCentered($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereContent($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereCreatedAt($value)
 * @method static \Illuminate\Database\Query\Builder|\App\Models\Slider whereUpdatedAt($value)
 * @mixin \Eloquent
 */
class Slider extends Model implements PresentableInterface
{

    /**
     * @var array
     */
    protected $guarded = ['id'];

    /**
     * @return HasOne
     */
    public function background(): HasOne
    {
        return $this->hasOne(SliderBackground::class);
    }

    /**
     * @return HasOne
     */
    public function button(): HasOne
    {
        return $this->hasOne(SliderButton::class);
    }

    /**
     * Return a created presenter.
     *
     * @return Presenter
     */
    public function getPresenter()
    {
        return app(SlidersPresenter::class, [$this]);
    }
}
