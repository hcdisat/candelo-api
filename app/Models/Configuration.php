<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class Configuration extends Model implements Transformable
{
    use TransformableTrait;

    /** @var array  */
    protected $guarded = ['id'];
}
