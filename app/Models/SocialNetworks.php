<?php namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Prettus\Repository\Contracts\Transformable;
use Prettus\Repository\Traits\TransformableTrait;

class SocialNetworks extends Model implements Transformable
{
    use TransformableTrait, SoftDeletes;

    /** @var array  */
    protected $guarded = ['id'];

    /** @var array  */
    protected $dates = ['deleted_at'];

}
