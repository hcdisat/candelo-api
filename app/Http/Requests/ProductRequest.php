<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ProductRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'category_id' => 'required',
            'name' => 'required',
            'description' => 'required',
            'price' => 'required|numeric|between:0,99999999.99',
            'image' => 'required|mimes:jpeg,bmp,png'
        ];

        if ($this->isMethod('PUT')) {
            unset($rules['name']);
            $rules['image'] = 'mimes:jpeg,bmp,png';
        }

        return $rules;
    }
}
