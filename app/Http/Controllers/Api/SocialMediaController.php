<?php namespace App\Http\Controllers\Api;

use App\Http\Requests\SocialMediaRequest;
use App\Repositories\Contracts\SocialMediaRepository;
use App\Repositories\Criteria\WithDeletedCriteria;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Intervention\Image\Exception\NotSupportedException;

class SocialMediaController extends Controller
{

    /** @var  SocialMediaRepository */
    private $_repository;

    /**
     * SocialMediaController constructor.
     * @param SocialMediaRepository $_repository
     */
    public function __construct(SocialMediaRepository $_repository)
    {
        $this->_repository = $_repository;
        $this->_repository->pushCriteria(app(WithDeletedCriteria::class, [$this->_repository]));
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()
            ->json($this->_repository->all(), 200);
    }

    /**
     * @param int $socialMedia
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $socialMedia)
    {
        return response()
            ->json($this->_repository->find($socialMedia), 200);
    }

    public function update(SocialMediaRequest $request, int $socialMedia)
    {
        $this->_repository->update($request->all(), $socialMedia);
        return response()->json([
            'data' => null,
            'message' => 'Social Network updated.',
            'status' => 200
        ], 200);
    }

    public function destroy(int $socialMedia)
    {
        $this->_repository->delete($socialMedia);
        return response()->json([
            'data' => null,
            'message' => 'Social Network deactivated.',
            'status' => 200
        ]);
    }

    /**
     * Not Implemented
     */
    public function store()
    {
        throw new NotSupportedException();
    }
}
