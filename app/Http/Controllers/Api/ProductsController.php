<?php namespace App\Http\Controllers\Api;

use App\Hcdisat\Services\ProductService;
use App\Hcdisat\Traits\UploadFileTrait;
use App\Http\Requests\ProductRequest;
use App\Jobs\CreateProductJob;
use App\Models\Product;
use App\Repositories\Contracts\ProductRepository;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\JsonResponse;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\UploadedFile;

class ProductsController extends Controller
{
    use UploadFileTrait;

    const IMG_LARGE = 360;
    const IMG_SMALL = 191.75;

    /**
     * @var ProductRepository
     */
    private $_repository;

    /**
     * ProductsController constructor.
     * @param ProductRepository $_repository
     */
    public function __construct(ProductRepository $_repository)
    {
        $this->_repository = $_repository;
    }

    /**
     * @return JsonResponse
     */
    public function index()
    {
        return $this->_repository->all();
    }

    /**
     * @param Product $product
     * @return Product
     */
    public function show(Product $product)
    {
        return $product;
    }

    /**
     * @param ProductRequest $request
     * @return JsonResponse
     * @throws FileNotFoundException
     */
    public function store(ProductRequest $request)
    {
        try {
            \Artisan::call('product:create', [
                'data' => $request->all()
            ]);

            return response()->json([
                'data' => null,
                'message' => 'product created.',
                'status' => 201
            ], 201);
        }
        catch(\Exception $ex) {
            return response()->json([
                'data' => null,
                'message' => $ex->getMessage(),
                'status' => 500
            ], 500);
        }
    }

    /**
     * @param ProductRequest $request
     * @param int $product
     * @return JsonResponse
     */
    public function update(ProductRequest $request, int $product)
    {
        try {
            \Artisan::call('product:update', [
                'data' => $request->all(),
                'productId' => $product
            ]);

            return response()->json([
                'data' => null,
                'message' => 'product updated.',
                'status' => 200
            ], 200);
        }
        catch(\Exception $ex) {
            return response()->json([
                'data' => null,
                'message' => $ex->getMessage(),
                'status' => 500
            ], 500);
        }
    }

    /**
     * @param int $product
     * @return JsonResponse
     */
    public function destroy(int $product)
    {
        \Artisan::call('product:destroy', [
            'productId' => $product
        ]);

        return response()->json([
            'data' => null,
            'message' => 'product deleted.',
            'status' => 200
        ], 200);
    }

    /**
     * @param Request $request
     * @param int $product
     * @param ProductService $service
     * @return JsonResponse
     */
    public function updateImage(Request $request, int $product, ProductService $service)
    {
        /** @var Product $product */
        $product = $this->_repository->find($product);
        if (!($request->image instanceof UploadedFile)) {
            return response()->json([
                'data' => $request->all(),
                'message' => 'image was not updated.',
                'status' => 500
            ], 500);
        }

        list($largePath, $shortPath) = $service->uploadImage(
            $product->name,
            $request->image
        );

        $product->update([
            'url_large' => str_replace(storage_path('app'), '', $largePath),
            'url_small' => str_replace(storage_path('app'), '', $shortPath),
            'image_large' => $largePath,
            'image_small' => $shortPath,
        ]);

        return response()->json([
            'data' => null,
            'message' => 'image updated.',
            'status' => 200
        ], 200);
    }
}