<?php namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Repositories\Contracts\CategoryRepository;

class CategoriesController extends Controller
{
    /**
     * @var CategoryRepository
     */
    protected $repository;

    /**
     * CategoriesController constructor.
     * @param CategoryRepository $repository
     */
    public function __construct(CategoryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->repository->all();
    }

    /**
     * @param int $category
     * @return mixed
     */
    public function show(int $category)
    {
        return $this->repository->find($category);
    }

    /**
     * @param CategoryRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(CategoryRequest $request)
    {
        $data = $request->all();
        $this->repository->create($data);
        return response()->json([
            'data' => null,
            'message' => 'Category created.',
            'status' => 201
        ], 201);
    }

    /**
     * @param CategoryRequest $request
     * @param int $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(CategoryRequest $request, int $category)
    {
        $this->repository
            ->update($request->all(), $category);

        return response()->json([
            'data' => null,
            'message' => 'Category updated.',
            'status' => 200
        ], 200);
    }

    /**
     * @param int $category
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(int $category)
    {
        $response = $this->repository->delete($category) > 0 ? [
            'data' => null,
            'message' => 'Category deleted.',
            'status' => 200
        ] : [
            'data' => null,
            'message' => 'Category cannot be deleted.',
            'status' => 402
        ];

        return response()
            ->json(
                $response,
                $response['status'] == 402
                    ? 402 : 200
            );
    }
}
