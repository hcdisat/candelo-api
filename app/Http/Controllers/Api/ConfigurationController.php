<?php namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\ConfigurationRequest;
use App\Repositories\Contracts\ConfigurationRepository;

class ConfigurationController extends Controller
{

    /**
     * @var ConfigurationRepository
     */
    private $_repository;

    /**
     * ConfigurationController constructor.
     * @param ConfigurationRepository $_repository
     */
    public function __construct(ConfigurationRepository $_repository)
    {
        $this->_repository = $_repository;
    }

    /**
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return response()
            ->json($this->_repository->all(), 200);
    }

    public function update(ConfigurationRequest $request, int $config)
    {
        $this->_repository->update($request->all(), $config);
        return response()->json([
            'data' => null,
            'message' => 'value updated.',
            'status' => 200
        ], 200);
    }

    /**
     * @param int $config
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(int $config)
    {
        return response()
            ->json($this->_repository->find($config), 200);
    }
}
