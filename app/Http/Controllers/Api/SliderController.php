<?php

namespace App\Http\Controllers\Api;

use App\Http\Requests\SliderRequest;
use App\Repositories\Contracts\SliderRepository;
use App\Http\Controllers\Controller;

class SliderController extends Controller
{
    /**
     * @var SliderRepository
     */
    private $repository;

    /**
     * SliderController constructor.
     * @param SliderRepository $repository
     */
    public function __construct(SliderRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @return mixed
     */
    public function index()
    {
        return $this->repository
            ->with('button')
            ->with('background')
            ->all();
    }

    /**
     * @param int $id
     * @return mixed
     */
    public function show(int $id)
    {
        return $this->repository
            ->with('button')
            ->with('background')
            ->find($id);
    }

    /**
     * @param SliderRequest $request
     * @return array
     */
    public function store(SliderRequest $request)
    {
        $data = $this->normalizeRequest($request);
        \Artisan::call('make:slider:create', ['data' => $data]);
        return ['created' => true];
    }

    /**
     * @param SliderRequest $request
     * @param int $slider
     * @return array
     */
    public function update(SliderRequest $request, int $slider)
    {
        \Artisan::call('make:slider:update', [
            'data' => $this->normalizeRequest($request),
            'sliderId' => $slider
        ]);
        return ['updated' => true];
    }

    /**
     * @param int $slider
     * @return array
     */
    public function destroy(int $slider)
    {
        $this->repository->delete($slider);
        return ['deleted' => true];
    }

    /**
     * @param SliderRequest $request
     * @return array
     */
    protected function normalizeRequest(SliderRequest $request):array
    {
        $button = [
            'show' => $request->show,
            'text' => $request->text,
            'url' => $request->url
        ];

        $data = $request->except(array_keys($button));
        $data['button'] = $button;
        return $data;
    }
}
