<?php namespace App\ViewPresenters;

use App\Models\SliderBackground;
use App\Models\SliderButton;
use Robbo\Presenter\Presenter;

/**
 * Class SlidersPresenter
 * @package App\ViewPresenters
 * @property SliderBackground $background
 * @property SliderButton $button
 */
class SlidersPresenter extends Presenter
{
    /**
     * @return string
     */
    public function presentBackGroundUrl(): string
    {
        return url(sprintf('/images/sliders/%s/%s.%s',
            $this->id,
            $this->background->name,
            $this->background->extension));
    }
}