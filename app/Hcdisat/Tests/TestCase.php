<?php namespace App\Hcdisat\Tests;

use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\WithoutMiddleware;

abstract class TestCase extends \TestCase
{
    use DatabaseMigrations, WithoutMiddleware;

    /**
     * @var array
     */
    protected $response;

    protected function setUp()
    {
        parent::setUp();
    }


    /**
     * @param array $order
     * @param string $jsonDataPath
     * @return mixed
     */
    protected function getData(array $order = [], string $jsonDataPath)
    {
        $json = \File::get(
            __DIR__
            .DIRECTORY_SEPARATOR
            .'fake_responses'
            .DIRECTORY_SEPARATOR
            .$jsonDataPath);

        $this->assertNotEmpty($json);


        $data = json_decode($json, true);
        $orderedData = [];

        if (!empty($order)) {
            foreach ($data as $key => $record) {
                foreach ($order as $item) {
                    $orderedData[$key][$item] = $data[$key][$item];
                }
            }
        }


        return empty($orderedData) ? $data : $orderedData;
    }

    public function generateData(string $model)
    {
        $data = $this->getModelData();
        foreach ($data as $item) {
            factory($model)->create($item);
        }
    }

    /**
     * @return array
     */
    protected abstract function getModelData();
}