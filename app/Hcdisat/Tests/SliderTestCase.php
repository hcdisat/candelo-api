<?php namespace App\Hcdisat\Tests;


use App\Models\Slider;
use App\Models\SliderButton;
use App\Models\SliderBackground;
use App\Hcdisat\TestUploadFileTrait;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class SliderTestCase extends \TestCase
{
    use DatabaseMigrations;

    protected $storageBase;

    protected static $imageId;

    public function setUp()
    {
        parent::setUp();
        if (!file_exists(config('testing.assets.slider_image'))) {
            copy(__DIR__.DIRECTORY_SEPARATOR.'Assets'.DIRECTORY_SEPARATOR.'some_promo.jpg',
                config('testing.assets.slider_image'));
        }
        $this->storageBase = storage_path()
            .DIRECTORY_SEPARATOR
            .'app'
            .DIRECTORY_SEPARATOR
            .'images'
            .DIRECTORY_SEPARATOR
            .'sliders'
            .DIRECTORY_SEPARATOR;
    }

    public function tearDown()
    {
        parent::tearDown();
        self::$imageId = null;
    }

    /**
     * generate a new slider
     * @return mixed
     */
    public function generateSlider()
    {
        $title = 'Call now!!';

        $slider = factory(Slider::class)->create([
            'title' => $title,
            'order' => 1,
            'boxed' => false,
            'centered' => true,
            'content' => 'Pellentesque habitant morbi tristique.',
            'created_at' => '2016-09-10 22:39:23',
            'updated_at' => '2016-09-10 22:39:23',
        ]);

        self::$imageId = self::$imageId ?? 0;
        ++self::$imageId;
        $ext = 'jpg';
        $background = factory(SliderBackground::class)->make([
            'name' => self::$imageId,
            'extension' => $ext,
            'path' => $this->storageBase.self::$imageId.DIRECTORY_SEPARATOR.self::$imageId.'.'.$ext,
            'url' => str_replace(storage_path('app/'), '',
                $this->storageBase.self::$imageId.DIRECTORY_SEPARATOR.self::$imageId.'.'.$ext),
            'created_at' => '2016-09-10 22:39:23',
            'updated_at' => '2016-09-10 22:39:23',
        ]);
        $button = factory(SliderButton::class)->make([
            'text' => 'Learn More!',
            'show' => 0,
            'url' => '',
            'created_at' => '2016-09-10 22:39:23',
            'updated_at' => '2016-09-10 22:39:23',
        ]);

        $slider->background()->save($background);
        $slider->button()->save($button);

        return $slider;
    }
}