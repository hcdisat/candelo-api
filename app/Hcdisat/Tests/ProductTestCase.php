<?php namespace App\Hcdisat\Tests;

use App\Models\Product;
use App\Hcdisat\Traits\UploadFileTrait;

class ProductTestCase extends TestCase
{
    use UploadFileTrait;

    /**
     * @var array
     */
    protected $response;

    protected function setUp()
    {
        parent::setUp();
        $order = [
            'id',
            'category_id',
            'name',
            'description',
            'price',
            'image_large',
            'image_small',
            'created_at',
            'updated_at'
        ];

        $this->response = $this->getData($order, 'product_response.json');
        $this->generateData(Product::class);
    }

    protected function getModelData()
    {
        return [
            [
                'category_id' => 23,
                'name' => 'voluptas',
                'description' => 'Ut est laboriosam odio iusto quam eum et minus. Esse eos maiores dolorem occaecati aut. Nemo in et nostrum.',
                'price' => "245.36",
                'image_large' => 'http://lorempixel.com/360/360/sports?15258',
                'image_small' => 'http://lorempixel.com/640/640/city?19936',
                "created_at" => "2016-10-30 12:54:14",
                "updated_at" => "2016-10-30 12:54:14",
            ],
            [
                'category_id' => 11,
                'name' => 'eveniet',
                'description' => 'Qui iste dicta mollitia alias omnis alias quaerat ut amet. In quo repudiandae illo in quaerat enim dolor',
                'price' => "319.45",
                'image_large' => 'http://lorempixel.com/360/360/food?56092',
                'image_small' => 'http://lorempixel.com/640/640/abstract?56762',
                "created_at" => "2016-10-30 12:54:14",
                "updated_at" => "2016-10-30 12:54:14",
            ]
        ];
    }
}