<?php namespace App\Hcdisat\Services;


use App\Hcdisat\Traits\UploadFileTrait;
use App\Models\Slider;
use App\Models\SliderBackground;
use App\Models\SliderButton;
use App\Repositories\Contracts\SliderRepository;

class SliderService
{
    use UploadFileTrait;

    const DISK = 'local';

    /**
     * @var SliderRepository
     */
    private $_repository;

    /**
     * SliderService constructor.
     * @param SliderRepository $repository
     */
    public function __construct(SliderRepository $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * @param array $dataOne
     * @return Slider
     * @internal param array $data
     */
    public function create(array $dataOne): Slider
    {
        $data = collect($dataOne);
        $image = $this->getImage($data);
        $button = $data->pull('button');
        $ext = $image->guessClientExtension();
        $slider = $this->_repository->create($data->toArray());
        $url = asset($image->storeAs("images/sliders/{$slider->id}", "{$slider->id}.{$ext}", self::DISK));

        $slider->background()->save(app(SliderBackground::class, [[
            'name' => $slider->id,
            'extension' => ".{$ext}",
            'url' => $url,
            'path' => storage_path('app'.DIRECTORY_SEPARATOR.$url)
        ]]));

        $slider->button()->save(app(SliderButton::class, [$button]));
        return $slider;
    }

    /**
     * @param array $data
     * @param int $slider
     * @return Slider
     */
    public function update(array $data, int $slider)
    {
        $data = collect($data);

        try {
            $image = $this->getImage($data);
            $ext = $image->guessClientExtension();

        } catch (\InvalidArgumentException $ex) {
            $image = null;
            $ext = $this->_repository->find($slider)->extension;
        }

        $button = $data->pull('button');
        $this->_repository->update($data->toArray(), $slider);

        /** @var Slider $target */
        $target = $this->_repository->find($slider);
        if (!is_null($image)) {
            $image->storeAs("images/sliders/{$target->id}", "{$target->id}.{$ext}", self::DISK);
            $target->background()->update([
                'name' => $target->id,
                'extension' => ".{$ext}",
            ]);
        }


        $target->button()->update($button);
        $target->load('button', 'background');
        return $target;
    }
}