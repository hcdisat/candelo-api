<?php namespace App\Hcdisat\Services;

use App\Hcdisat\Traits\UploadFileTrait;
use App\Models\Product;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;
use App\Repositories\Contracts\ProductRepository;

class ProductService
{
    use UploadFileTrait;

    const IMG_LARGE = 360;
    const IMG_SMALL = 191.75;

    /**
     * @var ProductRepository
     */
    private $_repository;

    /**
     * ProductsController constructor.
     * @param ProductRepository $_repository
     */
    public function __construct(ProductRepository $_repository)
    {
        $this->_repository = $_repository;
    }

    /**
     * @return ProductRepository
     */
    public function getRepository(): ProductRepository
    {
        return $this->_repository;
    }

    /**
     * @param ProductRepository $repository
     */
    public function setRepository(ProductRepository $repository)
    {
        $this->_repository = $repository;
    }

    /**
     * @param array $request
     * @return mixed
     */
    public function create(array $request)
    {
        $data = collect($request);
        $image = $this->getImage($data);

        list($largePath, $shortPath)
            = $this->uploadImage($data->get('name'), $image);

        $product = $this->_repository->create(array_merge(
            $data->all(), [
                'url_large' => asset(str_replace(storage_path('app'), '', $largePath)),
                'url_small' => asset(str_replace(storage_path('app'), '', $shortPath)),
                'image_large' => $largePath,
                'image_small' => $shortPath,
            ]
        ));

        return $product;
    }

    public function update(array $request, int $productId)
    {
        $data = collect($request);
        $image = $this->getImageIfExist($data);

        /** @var Product $product */
        $product = $this->_repository->find($productId);

        if (is_null($image)) { // no image we keep the old image
            $this->_repository->update($data->all(), $productId);
            return $product;
        }

        if ($data->has('name') && $data->get('name') != $product->name) {
            foreach (['image_large', 'image_small'] as $prop) {
                $newName = $data->get('name');
                $newPath = str_replace($product->name, $newName, $product->$$prop);
                if (!\File::exists($newPath)) {
                    \File::makeDirectory(dirname($newPath), 0775, true);
                }
                \File::move($product->$$prop, $newPath);
                $product->$$prop = $newPath;
            }

            if (!empty($prop)) {
                $product->save();
                unset($prop);
            }
        }

        return $product;
    }

    /**
     * @param int $productId
     * @return int
     */
    public function destroy(int $productId)
    {
        $product = $this->_repository
            ->find($productId);

        $this->removeImage([
            $product->image_large,
            $product->image_small
        ]);

        return $this->_repository->delete($productId);
    }

    /**
     * @param string $productName
     * @param UploadedFile $image
     * @return array
     */
    public function uploadImage(string $productName, UploadedFile $image)
    {
        $this->initializeStorage();

        $largePath = sprintf('%s%s%s.%s',
            $productName,
            DIRECTORY_SEPARATOR,
            $productName,
            $image->extension()
        );

        $shortPath = sprintf('%s%s%s%s%s.%s',
            $productName,
            DIRECTORY_SEPARATOR,
            'small',
            DIRECTORY_SEPARATOR,
            $productName,
            $image->extension()
        );

        $largePath = $this->getProductsStoragePath($largePath);
        $shortPath = $this->getProductsStoragePath($shortPath);

        $largeDir = explode(DIRECTORY_SEPARATOR, $largePath);
        $shortDir = explode(DIRECTORY_SEPARATOR, $shortPath);
        array_pop($largeDir);
        array_pop($shortDir);
        $largeDir = implode(DIRECTORY_SEPARATOR, $largeDir);
        $shortDir = implode(DIRECTORY_SEPARATOR, $shortDir);

        if (!\File::exists($largeDir)) {
            \File::makeDirectory($shortDir, 0755, true);
        }

        \Image::make($image->getRealPath())
            ->resize(self::IMG_LARGE, self::IMG_LARGE)->save($largePath);

        \Image::make($image->getRealPath())
            ->resize(self::IMG_SMALL, self::IMG_SMALL)->save($shortPath);

        return [
            $largePath,
            $shortPath
        ];
    }

    /**
     * @param $images
     */
    private function removeImage($images)
    {
        \File::delete($images);
    }
}