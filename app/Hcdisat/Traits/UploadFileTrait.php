<?php namespace App\Hcdisat\Traits;

use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Collection;

/**
 * Created by PhpStorm.
 * User: hcdisat
 * Date: 30/10/16
 * Time: 10:12 AM
 */
trait UploadFileTrait
{
    /**
     * mime eg. image/jpeg
     * @param string $path
     * @param string $name
     * @param string $mime
     * @return \Illuminate\Foundation\Application|mixed
     * @throws FileNotFoundException
     */
    public function prepareFileUpload(string $path, string $name, string $mime)
    {
        if (!file_exists($path)) {
            throw new FileNotFoundException("{$path} does not exists");
        }
//        $fInfo = finfo_open(FILEINFO_MIME_TYPE);
//        $mime = finfo_file($fInfo, $path);

        return app(UploadedFile::class, [
            $path,
            $name,
            $mime,
            filesize($path),
            null,
            true // for $test
        ]);
    }

    /**
     * @param Collection $data
     * @return UploadedFile
     */
    public function getImage(Collection $data): UploadedFile
    {
        if (!$data->has('image')) {
            throw new \InvalidArgumentException('Data does not contain an image.');
        }

        $image = $data->pull('image');

        if (!($image instanceof UploadedFile)) {
            throw new \InvalidArgumentException('Image is not valid.');
        }

        return $image;
    }

    /**
     * @param Collection $data
     * @return UploadedFile| null
     */
    public function getImageIfExist(Collection $data)
    {
        try {
            return $this->getImage($data);

        } catch (\Exception $ex) {
            return null;
        }
    }

    /**
     * @param string $subPath
     * @return string
     * @throws FileNotFoundException
     */
    public function getSlidersStoragePath(string $subPath = '')
    {
        $path = 'app'
            .DIRECTORY_SEPARATOR
            .'images'
            .DIRECTORY_SEPARATOR
            .'sliders';

        return $this->getStorageImagesPath($path, $subPath);
    }

    /**
     * @param string $subPath
     * @return string
     * @throws FileNotFoundException
     */
    public function getProductsStoragePath(string $subPath = '')
    {
        $path = 'app'
            .DIRECTORY_SEPARATOR
            .'images'
            .DIRECTORY_SEPARATOR
            .'products';

        return $this->getStorageImagesPath($path, $subPath);
    }

    public function getStorageImagesPath(string $base, $subPath)
    {
        if (strpos($base, '/') === 0) {
            $base = substr($base, 1);
        }

        if (strpos($subPath, '/') === 0) {
            $subPath = substr($subPath, 1);
        }

        $path = $base
            .DIRECTORY_SEPARATOR
            .$subPath;

        return storage_path(strtolower(str_replace(' ', '_', $path)));
    }

    /**
     * init storage
     */
    public function initializeStorage()
    {
        $sliders = $this->getSlidersStoragePath();
        $products = $this->getProductsStoragePath();

        foreach ([$sliders, $products] as $item) {
            if (!\File::exists($item)) {
                \File::makeDirectory($item, 0755, true);
            }
        }
    }
}