<?php namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SliderRepository
 * @package namespace App\Repositories;
 */
interface SliderRepository extends RepositoryInterface
{
    //
}
