<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface ConfigurationRepository
 * @package namespace App\Repositories\Contracts;
 */
interface ConfigurationRepository extends RepositoryInterface
{
    //
}
