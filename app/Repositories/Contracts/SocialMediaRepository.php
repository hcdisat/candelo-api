<?php

namespace App\Repositories\Contracts;

use Prettus\Repository\Contracts\RepositoryInterface;

/**
 * Interface SocialMediaRepository
 * @package namespace App\Repositories\Contracts;
 */
interface SocialMediaRepository extends RepositoryInterface
{
    //
}
