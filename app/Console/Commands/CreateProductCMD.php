<?php namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Hcdisat\Services\ProductService;

class CreateProductCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:create {data*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates a product';

    /**
     * @var ProductService
     */
    private $_service;


    /**
     * Create a new command instance.
     *
     * @param ProductService $service
     */
    public function __construct(ProductService $service)
    {
        parent::__construct();
        $this->_service = $service;
    }

    /**
     * Execute the console command.
     */
    public function handle()
    {
        $data = $this->argument('data');
        if (!$data) {
            throw new \InvalidArgumentException('Invalid data array.');
        }

        $this->_service->create($data);
    }
}
