<?php

namespace App\Console\Commands;

use App\Hcdisat\Services\SliderService;
use Illuminate\Console\Command;

class CreateSlider extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:slider:create
                            {data*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Creates an slider';

    /**
     * @var SliderService
     */
    private $_sliderService;

    /**
     * Create a new command instance.
     *
     * @param SliderService $sliderService
     */
    public function __construct(SliderService $sliderService)
    {
        parent::__construct();
        $this->_sliderService = $sliderService;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = $this->argument('data');
        if (!$data) {
            throw new \InvalidArgumentException('Invalid data array.');
        }

        $this->_sliderService->create($data);
    }
}
