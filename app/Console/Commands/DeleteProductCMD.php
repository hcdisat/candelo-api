<?php

namespace App\Console\Commands;

use App\Hcdisat\Services\ProductService;
use Illuminate\Console\Command;

class DeleteProductCMD extends Command
{
    const ProductId = 'productId';

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:destroy {productId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Deletes a product.';
    /**
     * @var ProductService
     */
    private $service;

    /**
     * Create a new command instance
     * @param ProductService $service
     */
    public function __construct(ProductService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $productId = $this->argument(self::ProductId);
        $this->service->destroy($productId);
    }
}
