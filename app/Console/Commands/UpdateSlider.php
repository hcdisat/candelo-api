<?php

namespace App\Console\Commands;

use App\Hcdisat\Services\SliderService;
use Illuminate\Console\Command;

class UpdateSlider extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:slider:update {data} {sliderId}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates an slider ';

    /**
     * @var SliderService
     */
    private $_service;

    /**
     * Create a new command instance.
     *
     * @param SliderService $service
     */
    public function __construct(SliderService $service)
    {
        parent::__construct();
        $this->_service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $data = $this->argument('data');
        if (!$data) {
            throw new \InvalidArgumentException('Invalid data array.');
        }

        $this->_service->update($data, $this->argument('sliderId'));
    }
}
