<?php

namespace App\Console\Commands;

use App\Hcdisat\Services\ProductService;
use Illuminate\Console\Command;

class UpdateProductCMD extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'product:update {productId} {data*}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Updates a Product';

    /**
     * @var ProductService
     */
    private $service;

    /**
     * Create a new command instance.
     *
     * @param ProductService $service
     */
    public function __construct(ProductService $service)
    {
        parent::__construct();
        $this->service = $service;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->argument('productId');
        $data = $this->argument('data');

        $this->service->update($data, $id);
    }
}
