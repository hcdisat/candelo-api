<?php

namespace App\Console;

use App\Console\Commands\CreateProductCMD;
use App\Console\Commands\CreateSlider;
use App\Console\Commands\DeleteProductCMD;
use App\Console\Commands\UpdateProductCMD;
use App\Console\Commands\UpdateSlider;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        CreateSlider::class,
        UpdateSlider::class,
        CreateProductCMD::class,
        UpdateProductCMD::class,
        DeleteProductCMD::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();
    }

    /**
     * Register the Closure based commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        require base_path('routes/console.php');
    }
}
