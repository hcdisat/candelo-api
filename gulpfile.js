const elixir = require('laravel-elixir');

require('laravel-elixir-vue');
var baseSrc = 'resources/assets/compiled';
var baseDest = 'public';
var vendor = '../vendor';

elixir(mix => {
    mix.sass('app.scss', baseSrc + '/css/app.css')

    /**
     * less
     */
        .less('admin-lte/AdminLTE.less', baseSrc + '/css/app-less.css')

        /**
         * Styles
         */
        .styles([
            '../compiled/css/app.css',
            '../compiled/css/app-less.css',
            '../compiled/css/keen-ui.min.css',
            '../vendor/bootstrap-fileinput/css/fileinput.css',
            'main.css',
        ], baseDest + '/css/app.css')

        /**
         * Webpack
         */
        .webpack('app.js', baseSrc + '/js/bundle.js')

        /**
         * Scripts
         */
        .scripts([
            '../compiled/js/bundle.js',
            '../compiled/js/app.min.js'
        ], baseDest + '/js/app.js')

        /**
         * Plugins
         */
        .scripts([
            '../vendor/bootstrap-fileinput/js/fileinput.js',
            '../vendor/bootstrap-fileinput/themes/fa/theme.js',
            '../vendor/bootstrap-fileinput/js/locales/es.js'
        ], baseDest + '/js/plugins.js');
});
